FROM golang:1.17 as go

WORKDIR /app

ENV GO111MODULE=on

COPY . .
RUN go build -o lgg_open_match_proxy cmd/proxy/main.go

ENTRYPOINT ["/app/lgg_open_match_proxy"]
