package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	proxyGrpc "gitlab.com/latency.gg/lgg-open-match-proxy/pkg/grpc"
	proxyHttp "gitlab.com/latency.gg/lgg-open-match-proxy/pkg/http"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"

	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"
)

const (
	lggClientApiUsernameEnv = "LGG_CLIENT_USERNAME"
	lggClientApiPasswordEnv = "LGG_CLIENT_PASSWORD"
)

// env vars for security purposes
var (
	lggClientApiUsername string
	lggClientApiPassword string
)

// program args
var (
	lggClientApiAddr      string
	requestTimeout        time.Duration
	selfEndpoint          string
	selfHttpPort          int
	selfGrpcPort          int
	openMatchHttpEndpoint string
	openMatchGrpcEndpoint string
)

func main() {
	parseProgramArgs()
	parseEnvVars()

	defer awaitTermination()

	startGrpcServer()
	startHttpServer()
}

func parseProgramArgs() {
	var lggClientApiAddrPtr = flag.String("lgg-client-endpoint", "https://client.latency.gg", "The address to the LatencyGG Client API.")

	var selfEndpointPtr = flag.String("self-endpoint", "0.0.0.0", "The host address to this server application.")
	var selfHttpPortPtr = flag.Int("self-http-port", 8080, "The http port.")
	var selfGrpcPortPtr = flag.Int("self-grpc-port", 8081, "The gRPC port.")

	var openMatchHttpEndpointPtr = flag.String("open-match-http-endpoint", "open-match-frontend.open-match.svc.cluster.local:51504", "The in-cluster address to the OpenMatch HTTP based Frontend service.")
	var openMatchGrpcEndpointPtr = flag.String("open-match-grpc-endpoint", "open-match-frontend.open-match.svc.cluster.local:50504", "The in-cluster address to the OpenMatch gRPC based Frontend service.")

	var requestTimeoutPtr = flag.Duration("request-timeout", 10*time.Second, "The amount of seconds until a request times out.")

	flag.Parse()

	lggClientApiAddr = *lggClientApiAddrPtr
	selfEndpoint = *selfEndpointPtr
	selfHttpPort = *selfHttpPortPtr
	selfGrpcPort = *selfGrpcPortPtr
	openMatchHttpEndpoint = *openMatchHttpEndpointPtr
	openMatchGrpcEndpoint = *openMatchGrpcEndpointPtr
	requestTimeout = *requestTimeoutPtr

	log.Printf("LatencyGG Client API address: %v\n", lggClientApiAddr)
	log.Printf("Open-Match HTTP endpoint: %v\n", openMatchHttpEndpoint)
	log.Printf("Open-Match gRPC endpoint: %v\n", openMatchGrpcEndpoint)
}

func parseEnvVars() {
	lggClientApiUsername = os.Getenv("LGG_CLIENT_USERNAME")
	if len(lggClientApiUsername) == 0 {
		missingEnvVar(lggClientApiUsernameEnv)
	}

	lggClientApiPassword = os.Getenv("LGG_CLIENT_PASSWORD")
	if len(lggClientApiPassword) == 0 {
		missingEnvVar(lggClientApiPasswordEnv)
	}
}

func startGrpcServer() {
	metricsProvider := lgg.NewHttpMetricsProvider(lggClientApiAddr, lggClientApiUsername, lggClientApiPassword, requestTimeout)
	locationProvider := lgg.NewHttpLocationProvider(lggClientApiAddr, lggClientApiUsername, lggClientApiPassword, requestTimeout)

	conn, err := grpc.Dial(openMatchGrpcEndpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	client := pb.NewFrontendServiceClient(conn)
	p := proxyGrpc.NewProxy(client, metricsProvider, locationProvider, nil)

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprint(":", selfGrpcPort))
		if err != nil {
			return
		}

		s := grpc.NewServer()
		pb.RegisterFrontendServiceServer(s, p)

		log.Printf("Starting gRPC proxy server at %v", lis.Addr())
		if err := s.Serve(lis); err != nil {
			return
		}
	}()
}

func startHttpServer() {
	metricsProvider := lgg.NewHttpMetricsProvider(lggClientApiAddr, lggClientApiUsername, lggClientApiPassword, requestTimeout)
	locationProvider := lgg.NewHttpLocationProvider(lggClientApiAddr, lggClientApiUsername, lggClientApiPassword, requestTimeout)

	client := &http.Client{Timeout: requestTimeout}
	handler := proxyHttp.NewProxy(client, metricsProvider, locationProvider, openMatchHttpEndpoint)

	r := mux.NewRouter()
	r.HandleFunc("/v1/frontendservice/tickets", handler.CreateTicket).Methods("POST")

	go func() {
		log.Printf("Starting HTTP proxy server at %v:%v\n", selfEndpoint, selfHttpPort)
		if err := http.ListenAndServe(fmt.Sprint(selfEndpoint, ":", selfHttpPort), r); err != nil {
			log.Fatal("ListenAndServe:", err)
		}
	}()
}

func awaitTermination() {
	c := make(chan os.Signal, 1)

	// subscribe to SIGINT
	signal.Notify(c, os.Interrupt)

	// and listen for a SIGINT event
	<-c
}

func missingEnvVar(name string) {
	log.Fatalf("No value specified in environment variable %v", name)
}
