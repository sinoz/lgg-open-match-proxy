package main

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	spec "gitlab.com/latency.gg/lgg-open-match-spec/pkg/pb"
	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"
)

func main() {
	// assuming that the proxy has been portforwarded
	conn, err := grpc.Dial("localhost:8081", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	client := pb.NewFrontendServiceClient(conn)

	extensions := make(map[string]*any.Any)

	ipAddr, err := ptypes.MarshalAny(&spec.IPAddress{
		Value: "<INSERT_A_CLIENTS_IP_ADDRESS_HERE>",
	})
	if err != nil {
		log.Fatal(err)
	}

	extensions["ip_address"] = ipAddr

	_, err = client.CreateTicket(context.Background(), &pb.CreateTicketRequest{
		Ticket: &pb.Ticket{
			Extensions: extensions,
		},
	})

	if err != nil {
		log.Fatal(err)
	}
}
