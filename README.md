# LGG Open-Match Proxy

An HTTP-and gRPC proxy that intercepts-and forwards Open-Match requests to allow latency based matchmaking, with Open-Match and LatencyGG.

## Why?

Requests for matchmaking are traditionally sent towards the frontend service of Open-Match Core, which are then processed-and evaluated.

![Game Services Architecture](https://open-match.dev/site/images/architecture.png)

However, in order to support latency based matchmaking, we need latency information that LatencyGG can provide. Due to the performance criteria of matchfunctions, which should be lightweight and non-blocking, it is not ideal to make calls towards LatencyGG inside a matchfunction. At the same time, it is important that LatencyGG is treated as an extension and so users should not manually have to build-in support for LatencyGG into their game frontend servers.

So the next best solution was to build a proxy that sits in between Open-Match Core-and the game frontend. 

The proxy stays true to the API contract of the Open-Match Core's frontend service, so that it is completely up to the end user if they wish calls to be made towards LatencyGG. This proxy can be (un)installed easily by adding-or deleting the proxy's deployment to/from the cluster. The minor downside of this is that if OpenMatch makes backwards compatibility breaking changes to the API, the proxy would have to be updated accordingly.

#### Request Prerequisite

In order for the proxy to be able to make calls to LatencyGG, the IP address of the game client's machine must be included in the request as an extension. The proxy also needs a set of locations of _interest_. This information is used during the matchmaking to match players together whose locations intersect. Without these two parameters, calls towards LatencyGG will **NOT** be made!

The `/api/` directory contains the `.proto` file that includes the API contract. See `/examples/` for an example client call.

## License

This project is available under the terms of the [WTFPL](http://www.wtfpl.net/) license. The license can be found in the `LICENSE` file.