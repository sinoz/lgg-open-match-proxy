package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/intercept"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"
	"open-match.dev/open-match/pkg/pb"
)

// See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec13.html
//
// "Hop-by-hop headers, which are meaningful only for a single
// transport-level connection, and are not stored by caches or
// forwarded by proxies."
var hopHeaders = []string{
	"Connection",
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"TE",
	"Trailers",
	"Transfer-Encoding",
	"Upgrade",
}

type Proxy struct {
	client *http.Client

	locationProvider lgg.LocationProvider
	metricsProvider  lgg.MetricsProvider

	destination string
}

func NewProxy(
	client *http.Client,
	metricsProvider lgg.MetricsProvider,
	locationProvider lgg.LocationProvider,
	destination string,
) *Proxy {
	return &Proxy{
		client,
		locationProvider,
		metricsProvider,
		destination,
	}
}

func (p *Proxy) CreateTicket(wr http.ResponseWriter, req *http.Request) {
	bodyBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		wr.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}

	createTicketReq := &pb.CreateTicketRequest{}
	targetUrl := fmt.Sprint(p.destination, "/v1/frontendservice/tickets")

	err = json.Unmarshal(bodyBytes, createTicketReq)
	if err != nil {
		wr.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return
	}

	err = intercept.Do(createTicketReq.Ticket, p.metricsProvider, p.locationProvider)
	if err != nil {
		wr.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}

	err = p.forwardRequest(wr, req, http.MethodPost, targetUrl, createTicketReq)
	if err != nil {
		wr.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
}

func (p *Proxy) forwardRequest(
	wr http.ResponseWriter,
	req *http.Request,
	method string,
	targetUrl string,
	createTicketReq *pb.CreateTicketRequest,
) error {
	// marshal the possibly updated request
	reqBytes, err := json.Marshal(createTicketReq)
	if err != nil {
		return err
	}

	// deletes all of the hop-headers that are not to be
	// proxied / forwarded to the next destination.
	for _, h := range hopHeaders {
		req.Header.Del(h)
	}

	// rebuild the request to the designated endpoint
	forwardReq, err := http.NewRequest(method, targetUrl, bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}

	// copy over the remaining headers
	for headerName, valueList := range req.Header {
		for _, v := range valueList {
			forwardReq.Header.Add(headerName, v)
		}
	}

	// and now we send in said request
	resp, err := p.client.Do(forwardReq)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	// strip all of the hop-headers from the response
	for _, h := range hopHeaders {
		resp.Header.Del(h)
	}

	// include the 'Forwarded' header
	wr.Header().Add("Forwarded", fmt.Sprintf("by=unknown;for=%v;proto=http", p.destination))

	// copy over the remaining headers from the call
	// response into the actual response
	for headerName, valueList := range resp.Header {
		for _, v := range valueList {
			wr.Header().Add(headerName, v)
		}
	}

	// include the status code
	wr.WriteHeader(resp.StatusCode)

	// and also stream the response body
	io.Copy(wr, resp.Body)

	return nil
}
