package http

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"
	spec "gitlab.com/latency.gg/lgg-open-match-spec/pkg/pb"
	"open-match.dev/open-match/pkg/pb"
)

func TestProxy(t *testing.T) {
	client := &http.Client{}

	locationProvider := lgg.NewDummyLocationProvider(nil)
	metricsProvider := lgg.NewDummyMetricsProvider(lgg.Metrics{})

	// spin up the sink http server that is to represent the openmatch frontend service
	s := httptest.NewServer(
		http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			bytes, err := ioutil.ReadAll(r.Body)
			if err != nil {
				t.Fail()
				return
			}

			rw.WriteHeader(http.StatusAccepted)
			rw.Write(bytes)
		}),
	)

	defer s.Close()

	p := NewProxy(client, metricsProvider, locationProvider, s.URL)

	http.HandleFunc("/", p.CreateTicket)
	go func() {
		http.ListenAndServe(":8080", nil)
	}()

	// give the http server some time to boot up
	time.Sleep(2 * time.Second)

	// now we build up the actual ticket request
	extensions := make(map[string]*any.Any)

	ipAddr, err := ptypes.MarshalAny(&spec.IPAddress{
		Value: "62.163.0.35",
	})
	if err != nil {
		t.Error(err)
	}

	extensions["ip_address"] = ipAddr

	req := &pb.CreateTicketRequest{
		Ticket: &pb.Ticket{
			Extensions: extensions,
		},
	}

	// marshal the ticket into json
	reqBytes, err := json.Marshal(req)
	if err != nil {
		t.Fatal(err)
	}

	httpReq, err := http.NewRequest(http.MethodPost, "http://127.0.0.1:8080", bytes.NewBuffer(reqBytes))
	if err != nil {
		t.Fatal(err)
	}

	response, err := client.Do(httpReq)
	if err != nil {
		t.Fatal(err)
	}

	if response.StatusCode != http.StatusAccepted {
		t.Fail()
	}
}
