package intercept

import (
	"os"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"
	spec "gitlab.com/latency.gg/lgg-open-match-spec/pkg/pb"
	"open-match.dev/open-match/pkg/pb"
)

func TestIntercept(t *testing.T) {
	var (
		username = os.Getenv("LGG_CLIENT_USERNAME")
		password = os.Getenv("LGG_CLIENT_PASSWORD")
	)

	var locationProvider = lgg.NewHttpLocationProvider("client.latency.gg", username, password, 5*time.Second)
	var metricsProvider = lgg.NewHttpMetricsProvider("client.latency.gg", username, password, 5*time.Second)

	extensions := make(map[string]*any.Any)

	ipAddr, err := ptypes.MarshalAny(&spec.IPAddress{
		Value: "62.163.0.35",
	})
	if err != nil {
		t.Error(err)
		return
	}

	extensions["ip_address"] = ipAddr

	ticket := &pb.Ticket{
		Id:         "123",
		Extensions: extensions,
	}

	err = Do(ticket, metricsProvider, locationProvider)
	if err != nil {
		t.Error(err)
		return
	}

	calculationSet := &spec.CalculationSet{}
	calculationsExt := ticket.Extensions["lgg_calculations"]

	err = ptypes.UnmarshalAny(calculationsExt, calculationSet)
	if err != nil {
		t.Error(err)
		return
	}

	if len(calculationSet.Calculations) == 0 {
		t.Fail()
	}
}
