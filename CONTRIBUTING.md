# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue before making a change.

### Synchronizing your fork with upstream

#### Creating a new branch

First make sure that you are on your fork's master branch:

```bash
git checkout master
```

And have upstream added as a remote:

```bash
git remote add upstream https://gitlab.com/latency.gg/lgg-open-match-proxy
```

And then pull all changes from upstream onto your local setup:

```bash
git pull upstream master
```

And finally push your locally updated master to your origin:

```bash
git push origin master
```

A branch can then be created-and switched to:

```bash
git checkout -b my-new-branch
```

## Compiling Protobuf

```sh
protoc -I=api --go_out=. ./api/intercept.proto
```